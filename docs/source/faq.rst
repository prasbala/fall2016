FAQ
====

Do I need to buy a textbook?
----------------------------------------------------------------------

No, the resources will be provided for every unit. However, there are
some optional textbooks if you would like to purchase one.

#. “Taming The Big Data Tidal Wave: Finding Opportunities in Huge Data
   Streams with Advanced Analytics”, Bill Franks Wiley ISBN:
   978-1-118-20878-6
#. “Doing Data Science: Straight Talk from the Frontline”, Cathy O'Neil,
   Rachel Schutt, O'Reilly Media, ISBN 978-1449358655

Representative Bibliography
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. `Big data: The next frontier for innovation, competition, and
   productivity <http://www.mckinsey.com/insights/business_technology/big_data_the_next_frontier_for_innovation>`__
#. `Big Data Spring 2015
   Class <https://bigdatacoursespring2015.appspot.com>`__

Where is the official IU calendar for the Fall?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Please follow this
`link <http://registrar.indiana.edu/official-calendar/official-calendar-fall.shtml>`__

How to write a research article on computer science
----------------------------------------------------------------------

#. `http://www.wv.inf.tu-dresden.de/Teaching/SS-2012/howto/writing.pdf <http://www.wv.inf.tu-dresden.de/Teaching/SS-2012/howto/writing.pdf>`__
#. `https://globaljournals.org/guidelines-tips/research-paper-publishing <https://globaljournals.org/guidelines-tips/research-paper-publishing>`__ 
#. `http://www.cs.columbia.edu/~hgs/etc/writing-style.html <http://www.cs.columbia.edu/~hgs/etc/writing-style.html>`__ 
#. `https://www.quora.com/How-do-I-write-a-research-paper-for-a-computer-science-journal <https://www.quora.com/How-do-I-write-a-research-paper-for-a-computer-science-journal>`__ 

How to you use bibliography managers jabref & endnode or mandalay
----------------------------------------------------------------------

#. `http://www.jabref.org/ <http://www.jabref.org/>`__ 
#. `http://endnote.com/ <http://endnote.com/>`__ 
#. `http://libguides.utoledo.edu/c.php?g=284330&p=1895338 <http://libguides.utoledo.edu/c.php?g=284330&p=1895338>`__ 
#. `https://www.mendeley.com/ <https://www.mendeley.com/>`__ 
#. `https://community.mendeley.com/guides/using-citation-editor/05-creating-bibliography <https://community.mendeley.com/guides/using-citation-editor/05-creating-bibliography>`__ 

Plagiarism test and resources related to that
----------------------------------------------------------------------

#. `https://www.grammarly.com/plagiarism-checker <https://www.grammarly.com/plagiarism-checker>`__ 
#. `http://turnitin.com/ <http://turnitin.com/>`__ 
#. `http://www.plagscan.com/plagiarism-check/ <http://www.plagscan.com/plagiarism-check/>`__

